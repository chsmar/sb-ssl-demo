#Spring boot web app and SSL

# Create source code
Generate self-signed certificate
```
keytool -genkey -alias tomcat -storetype PKCS12 -keyalg RSA -keysize 2048 -keystore keystore.p12 -validity 3650
```
More about SSL: https://www.drissamri.be/blog/java/enable-https-in-spring-boot

# Create Bitbucket repository
Login Bitbucket
Create repository

# Init Git
```
git init
git remote add origin https://MarcialChoque@bitbucket.org/MarcialChoque/sb-ssl-demo.git
git config user.email "marcial.choque@hotmail.com"
git config user.name "MarcialChoque"
```

# Add source
```
git add src/*
```

# Commit
```
git commit -m "Committing code"
git push -u origin master
```
