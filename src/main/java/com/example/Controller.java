package com.example;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by 2190 on 4/3/2016.
 */

@RestController
@RequestMapping("/")
public class Controller {

    @RequestMapping(method = RequestMethod.GET)
    public String hello() {
        return "Hello World!";
    }
}
